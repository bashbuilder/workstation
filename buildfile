#!/bin/bash

export bash_builder_prefix=/opt/bashbuilder/buildash
. ${bash_builder_prefix}/lib/builder.sh

maintainer Alexey Fedorov waterlink000+buildash@gmail.com

# global config
environ arch $(arch)
environ work_path $PWD

. lib/future $@

# add postgres 9.2 to package manager
task enable_postgres_9.2 {
	add templates/pgdg.list ~/pgdg.list
	run sudo cp ~/pgdg.list /etc/apt/sources.list.d/
	run wget -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
}

# init package manager
task init_package_manager {
	package-update
}

# === software block ===
task software_block {
	package-install vim htop chromium-browser zsh curl git libreoffice synapse openssh-server
}

# === zsh block ===
task install_zsh {
	# install oh my zsh
	run curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

	# zshrc config
	add templates/zshrc ~/.zshrc
}

# === ST3 block ===
task install_st3 {
	# config
	declare -A st3_arch
	st3_arch=( ["i386"]="i386" ["i686"]="i386" ["x86_64"]="amd64" )
	environ st3_build 3047
	environ st3_package sublime-text_build-${st3_build}_${st3_arch[${arch}]}.deb
	environ st3_url http://c758482.r82.cf2.rackcdn.com/${st3_package}

	# install
	run cd /tmp; wget -c ${st3_package}; sudo dpkg -i ${st3_package}; cd ${work_path}
}

# === Skype block ===
task install_skype {
	# config
	declare -A skype_arch
	skype_arch=( ["i386"]="32" ["i686"]="32" ["x86_64"]="64" )
	environ skype_build beta
	environ skype_package getskype-linux-${skype_build}-${skype_arch[${arch}]}
	environ skype_url http://www.skype.com/go/${skype_package}

	#install
	cd /tmp; wget -c ${skype_url}; sudo dpkg -i ${skype_package}; cd ${work_path}
}

# === Google Chrome block ===
task install_chrome {
	# config
	declare -A chrome_arch
	chrome_arch=( ["i386"]="i386" ["i686"]="i386" ["x86_64"]="amd64" )
	environ chrome_build stable_current
	environ chrome_package google-chrome-${chrome_build}_${chrome_arch[${arch}]}.deb
	environ chrome_url https://dl.google.com/linux/direct/${chrome_package}

	# install
	cd /tmp; wget -c --no-check-certificate ${chrome_url}; sudo dpkg -i ${chrome_package}; cd ${work_path}
}

# === postgresql block (9.1, 9.2) ===
task install_postgress {
	# config
	pg_versions=( 9.1 9.2 )
	pg_packages=( postgresql postgresql-contrib postgresql-server-dev )

	# install pgs
	for pg_version in ${pg_versions[*]}; do
		for pg_package in ${pg_packages[*]}; do
			package-install ${pg_package}-${pg_version}
		done
	done
}

# === rvm block ===
task install_rvm {
	# config rvm
	environ rvm_url https://get.rvm.io
	environ rvm_head -s stable
	environ rvm_ruby --ruby=2.0.0
	environ rvm_rails --rails

	# dependencies
	package-install libxml2-dev libxslt1-dev

	# gemrc config
	add templates/gemrc ~/.gemrc

	# install rvm
	run [ -d ~/.rvm ] || curl -L ${rvm_url} | bash ${rvm_head} ${rvm_ruby} ${rvm_rails}
}

task_rule after_enable_postgres_9.2 {
	task_run_force init_package_manager
}

task_rule before_install_postgress {
	task_run enable_postgres_9.2
}

task_rule before_software_block {
	task_run init_package_manager
}

task_rule before_install_zsh {
	task_run software_block
}

task_rule before_install_rvm {
	task_run software_block
}

task all {
	echo_i "Running all tasks"
}

task_rule after_all {
	task_run install_zsh
	task_run install_st3
	task_run install_skype
	task_run install_chrome
	task_run install_postgress
	task_run install_rvm
}

run_tasks $@

